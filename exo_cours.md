# EXERCICE COURS

### 1) Ecrire une requête qui affiche les champs : restaurant_id, name, borough & cuisine pour tous les documents de la collection restaurant
```
	db.restaurants.find({},{"restaurant_id" : 1,"name" : 1,"borough" : 1, "cuisine" : 1 });  
```
### 2) Afficher les champs restaurant_id, name, borough & cuisine, mais exclure le champ _id for all the documents in the collection restaurant.
```
	db.restaurants.find({},{"restaurant_id" : 1,"name" : 1,"borough" : 1, "cuisine" : 1 , "_id":0});
```
### 3) Afficher les champs restaurant_id, name, borough & zip code, exclure _id
```
	db.restaurants.find({},{"restaurant_id" : 1,"name" : 1,"borough" : 1, "address.zipcode" : 1 , "_id":0});
```

### 4) Tous les restaurants dans le borough Bronx \#filter
```
	db.restaurants.find({borough:'Bronx'},{});
```
### 5) Afficher les 5 premiers restaurants avec le borough Bronx. Utiliser le retour avec .limit()
```
	db.restaurants.find({borough:'Bronx'},{}).limit(5);
```
### 6) 5 restaurants suivants après avoir sauté les 5 premiers dans le borough Bronx #skip
```
	db.restaurants.find({borough:'Bronx'},{}).skip(5).limit(5);
```
### 7) Dans filter utiliser $elemMatch et $gt $lt, 
###    Les restaurants avec un meilleur score que 90, 
###    Score plus que 80 mais < 100, 
###    Restaurants avec locate latitude less than -95.754168

```
		db.restaurants.find({"grades.score":{$gt:90}},{});

		db.restaurants.find({"grades.score":{$gt:80},"grades.score":{$lt:100}},{});

		db.restaurants.find({"address.coord.0":{$lt:-95.754168}},{});
```
### 8) Ne prepare aucun repas (cuisine) de type 'American' et score > 70 & lattitude < -65.754168
```
	db.restaurants.find({cuisine : {$ne:'American '}, "grades.score":{$lt:70}, "address.coord.0":{$lt:-65.754168}},{});
```
### 9) Ne prepare aucune repas (cuisine) de type 'American' et un grade de 'A' et n’appartient pas a Brooklyn
```
	db.restaurants.find({cuisine : {$ne:'American '}, "grades.grade":'A', borough:{$ne:'Brooklyn'}},{});
```
### 10) Affichage en fonction de type de repas (cuisine) dans l’ordre descendant
```
	db.restaurants.aggregate([{$sort:{"cuisine":-1}}])
```

### 11) Trouver l'ID de restaurant, le nom, l'arrondissement et la cuisine des restaurants qui contiennent «Wil» comme trois premières lettres de son nom
```
	db.restaurants.find({"name":/^Wil/},{"restaurant_id":1, "name":1, "borough":1, "cuisine":1, "_id":1})
```

### 12) Trouver l'ID de restaurant, le nom, le quartier et la cuisine des restaurants qui contiennent «ces» comme trois dernières lettres pour son nom.
```
	db.restaurants.find({"name":/ces$/},{"restaurant_id":1, "name":1, "address.street":1, "cuisine":1, "_id":1})
```

### 13) Trouver l'ID de restaurant, le nom, l'arrondissement et la cuisine des restaurants qui contiennent «Reg» comme trois lettres quelque part dans son nom
```
	db.restaurants.find({"name":/Reg/},{"restaurant_id":1, "name":1, "borough":1, "cuisine":1, "_id":1})
```
