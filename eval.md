# Évaluation

### 1) Faire une DB user supprimer et recréer cette dernière. Faire un check de la base de données vous trouvez

* Pour créer la DB

```
use user
```

* Pour supprimer

```
db.dropDatabase(User);
```

* Pour consulter la base

```
db
```

### 2) Faire une collection clients dans user et peupler le base.json Check si le doc est bien inséré

* Création de la collection

```
db.createCollection('clients');
```

* Pour peupler la collection

```
db.clients.insert({ "firstName":"John",
  "lastName":"West",
  "email":"john.west@mail.com",
  "phone":"032345432134",
  "BusinessType": ["Sell", "Sugar", "Drinks"],
  "Reference":100,
  "Company":"Coca-Cola"}
);
```
Il y a aussi :

```
florianmarchoux@MBP-de-Florian Eval % mongoimport -d user -c clients base.json
```

* Pour voir si les données sont bien inséré 

```
db.clients.find();
```

### 3)  Faire une collection nommée transactions dans user et peupler les données du json, trans.json

```
db.createCollection('transactions');



db.user.insert([
{
    "Id": 100,
    "Name": "John",
    "TransactionId": "tran1",
  "Transaction": [
    {
    "ItemId":"a100",
    "price": 200
    },
    {
    "ItemId":"a110",
    "price": 200
    }
  ],
  "Subscriber": true,
  "Payment": {
    "Type": "Credit-Card",
    "Total": 400,
    "Success": true
  },
  "Note": "1st Complete Record"
},
{
    "Id": 101,
    "Name": "Tom",
    "TransactionId": "tran2",
  "Transaction": [
    {
    "ItemId":"a100",
    "price": 200
    },
    {
    "ItemId":"a110",
    "price": 200
    }
  ],
  "Subscriber": true,
  "Payment": {
    "Type": "Debit-Card",
    "Total": 400,
    "Success": true
  },
  "Note":null
},
{
    "Id": 102,
    "Name": "Margaret",
    "TransactionId": "tran3",
  "Transaction": [
    {
    "ItemId":"a100",
    "price": 200
    },
    {
    "ItemId":"a110",
    "price": 200
    }
  ],
  "Subscriber": true,
  "Payment": {
    "Type": "Credit-Card",
    "Total": 400,
    "Success": true
  }
},
{
    "Id": 103,
    "Name": "Dylan",
    "TransactionId": "tran4",
  "Transaction": [
    {
    "ItemId":"a100",
    "price": 200
    },
    {
    "ItemId":"a110",
    "price": 200
    }
  ],
  "Subscriber": true,
  "Payment": null,
  "Note": "Payment is Null"
}
]);
```

* Mettre les données avec le même fichier, trans.json

```
florianmarchoux@MBP-de-Florian Eval % mongoimport --db user --collection transactions trans.json
```

* Upsert les données du fichier appelé trans_up.json

```
db.user.update(
upsert:{
[
{
    "Id": 100,
    "Name": "ZZZZZZ",
    "TransactionId": "tran1",
  "Transaction": [
    {
    "ItemId":"a100",
    "price": 2000000000
    },
    {
    "ItemId":"a110",
    "price": 200  
    }
  ],
  "Subscriber": true,
  "Payment": {
    "Type": "Credit-Card",
    "Total": 400,
    "Success": true
  },
  "Note": "1st Complete Record"
},
{
    "Id": 101,
    "Name": "Tom",
    "TransactionId": "tran2",
  "Transaction": [
    {
    "ItemId":"a100",
    "price": 200
    },
    {
    "ItemId":"a110",
    "price": 200  
    }
  ],
  "Subscriber": true,
  "Payment": {
    "Type": "Debit-Card",
    "Total": 400,
    "Success": true
  },
  "Note":null
},
{
    "Id": 102,
    "Name": "Margaret",
    "TransactionId": "tran3",
  "Transaction": [
    {
    "ItemId":"a100",
    "price": 200
    },
    {
    "ItemId":"a110",
    "price": 200  
    }
  ],
  "Subscriber": true,

  "Payment": {
    "Type": "Credit-Card",
    "Total": 400,
    "Success": true
  }
},
{
    "Id": 103,
    "Name": "Dylan",
    "TransactionId": "tran4",
  "Transaction": [
    {
    "ItemId":"a100",
    "price": 200
    },
    {
    "ItemId":"a110",
    "price": 200  
    }
  ],
  "Subscriber": true,
  "Payment": null,
  "Note": "Payment is Null"
},
{
    "Id": 104,
    "Name": "Oliver",
    "TransactionId": "tran5",
  "Transaction": [
    {
    "ItemId":"a100",
    "price": 200
    },
    {
    "ItemId":"a110",
    "price": 200  
    }
  ],
  "Subscriber": true,

  "Payment": {
    "Total": 400,
    "Success": true
  },
  "Note": "Payment Type is missing"
},
{
    "Id": 105,
    "Name": "Sarah",
    "TransactionId": "tran6",
  "Transaction": [
    {
    "ItemId":"a100",
    "price": 200
    },
    {
    "ItemId":"a110",
    "price": 200
    }
  ],
  "Subscriber": true,
  "Note": "Payment is missing"
}
]}

);
```

### 5)  Parcourir les résultats avec le nom Tom

```
db.transactions.find({"Name" : "Tom"}, {});
```

* Parcourir les résultats avec le montant total du paiement
(Payment.Total) est 400

```
db.transactions.find({
"Payment": {
            $elemMatch:{
                "Total": 400
                }
            }
            });

```

* Parcourir les résultats avec le prix (Transaction.price) supérieur à 400

```
db.transactions.find({
    "Transaction": {
        $elemMatch:{
            "price":{
                $not:{$lt:400},
                $gt:400
            }
        }
    }
},{});
```

* Parcourir les résultats où Note est nul ou la clé elle-même est manquante

```
db.transactions.find({
    $or: [
        {"Note":null},
        {"Note":{$exists: false}}
    ]
});

```

* Parcourir les résultats ou Note existe et sa valeur est nulle

```
db.transactions.find({
    $and: [
        {"Note":null }, 
        {"Note":{$exists: true}} 
    ]
});
```

* Parcourir les résultats où la clé Note n'existe pas.

```
db.transactions.find({Note:{$exists:0}},{});
```

### 6) Aggregation

* Calcul du montant total de la transaction en additionnant Payment.Total dans tous les docs

```
db.transactions.aggregate({
    $group: {
        _id: null,
        total: {
            $sum: "$Payment.Total"
        }
    }
});
```
* Chercher le prix total par doc en additionnant les valeurs de prix dans le tableau Transaction (Transaction.price)

```
db.transactions.aggregate({
    $unwind{
        _id: "$Id",
        price: {
            $sum: "$Transaction.price"
        }
    }
});
```

* Calcul du total des paiements (Payment.Total) pour chacun des types de paiement (Payment.Type)

```
```

* Obtenir l'ID max

```
db.transactions.aggregate({
    $group: {
        _id: null,
        IdMax: {
            $max: "$Id"
        }
    }
});
```

* Obtenir le prix maximum (Transaction.price)

```
db.transactions.aggregate({
    $group: {
        _id: null,
        MaxPrice: {
            $max: "$Transaction.price"
        }
    }
});
```

### 7) CRUD

* Insérez un insert.txt

```
florianmarchoux@MBP-de-Florian Eval % mongoimport --db user --collection transactions insert.txt
```
OU
```
db.transactions.insert({
    "Id": 110,
    "Name": "Inserted Record",
    "TransactionId": "tranNew1",
  "Transaction": [
    {
    "ItemId":"c324",
    "price": 456
    },
    {
    "ItemId":"d456",
    "price": 543
    }
  ],
  "Subscriber": false,
  "Payment": {
    "Type": "Debit-Card",
    "Total": 999,
    "Success": true
  },
  "Note": "Hello World"
})
```

* Mettre a jour le nouveau inséré ci-dessus. Make Name = ’Mise à jour
d’enregistrement’ & Note = ’Mise à jour!’

```
db.transactions.updateOne(
   { Id: "110" },
   {    
        $set: { "Name": "Mise à jour d’enregistrement", 
                "Note": "Mise à jour!" },
        $currentDate: { lastModified: true }
    }
);
```

* Supprimez le nouveau inséré ci-dessus en utilisant Id

```
db.transactions.remove( { Id : 110 } );

```

### 8) Droits

* Créez un utilisateur en lecture seule qui peut interroger les enregistrements des collections de toutes les bases de données

2 solutions

```
db.createUser({
      user: "UserLecture",
      pwd: "123Soleil",
      roles: [
         {role: "read",
         db: "admin"}
      ]
});
```
OU

```
db.createUser({
      user: "UserLecture",
      pwd: "123Soleil",
      roles: [
            { role: "read", db: "clients" },
            { role: "read", db: "transactions" }
      ]
});
```

* Créez un utilisateur ayant l’accès écriture qui peut créer des collections et effectuer des opérations CRUD dans toutes les collections

```
db.createUser(
  {
    user: "AlexTérieur",
    pwd: "456avouslasuite",
    roles: [
       { role: "userAdminAnyDatabase", db: "admin" }
    ]
  }
)
````

*  Créez un utilisateur qui peut faire toutes les opérations d'écriture et gestion des droits dans la base de données user et en lecture seule pour les autres bases

```
db.createUser(
  {
    user: "AlainTérieur",
    pwd: "789àvousl'honneur",
    roles: [
       { role: "userAdminAnyDatabase", db: "user" }
    ]
  }
)
````
