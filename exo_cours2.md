# EXERCICE COURS 2

### 1) Afficher tous les documents dans les restaurants de la collection

```
db.restaurants.find();
``` 

### 2) Afficher les champs restaurant_id, nom, arrondissement et cuisine pour tous les documents de la collection restaurant.

```
db.restaurants.find({},{"restaurant_id":1, "name":1, "borough":1, "cuisine":1});
```

### 3) Afficher les champs restaurant_id, nom, arrondissement et cuisine, mais excluez le champ _id pour tous les documents de la collection restaurant. 

```
db.restaurants.find({},{"restaurant_id":1, "name":1, "borough":1, "cuisine":1, "_id":0});
```

### 4) Afficher les champs restaurant_id, nom, arrondissement et code postal, mais excluez le champ _id pour tous les documents de la collection restaurant. 

```
db.restaurants.find({},{"restaurant_id":1, "name":1, "borough":1, "address.zipcodes":1, "_id":0});
```

### 5) Afficher tous les restaurants qui se trouvent dans l'arrondissement du Bronx. 

```
db.restaurants.find({"borough" : "Bronx"}, {});
```

### 6) Afficher les 5 premiers restaurants qui se trouvent dans l'arrondissement du Bronx avec le .limit() . 

```
db.restaurants.find({"borough" : "Bronx"}, {}).limit(5);
```

### 7) Afficher les 5 restaurants suivants après avoir sauté les 5 premiers qui se trouvent dans l'arrondissement du Bronx #skip . 

```
db.restaurants.find({"borough" : "Bronx"}, {}).skip(5).limit(5);
```

### 8) Trouver les restaurants qui ont obtenu un score supérieur à 90 avec $elemMatch et $gt $lt .

```
db.restaurants.find({
    "grades": {
        $elemMatch:{
            "score":{
                $not:{$lt:90},
                $gt:90
            }
        }
    }
},{});
```

### 9) Trouver les restaurants qui ont obtenu un score, plus de 80 mais moins de 100 avec $elemMatch et $gt $lt . 

```
db.restaurants.find({
    "grades": {
        $elemMatch:{
            "score":{
                $gt:80,
                $lt:100
            }
        }
    }
},{});
```

### 10) Trouver les restaurants qui se situent à une latitude inférieure à -95.754168 avec $elemMatch et $gt $lt . 

```
db.restaurants.find({
    "address.coord": {
        $elemMatch:{
            $not:{$gt:-95.754168},
            $lt: -95.754168
        }
    }
},{});
```

### 11) Trouver les restaurants qui ne préparent pas de cuisine "américaine" et dont la note est supérieure à 70 et la latitude inférieure à -65,754168. 

```
db.restaurants.find(
    {$and:[
        {"cuisine" : {$ne :"American "}},
        {"grades.score" : {$gt : 70}},
        {"address.coord" : {$lt : -65.754168}}
    ]}
);
```

### 12) Trouver les restaurants qui ne préparent pas de cuisine "américaine" et qui ont obtenu un score supérieur à 70 
et qui sont situés dans la longitude inférieure à -65,754168 sans utiliser l'opérateur $and. 

```
db.restaurants.find({
    "cuisine" : {$ne : "American "},
    "grades.score" :{$gt: 70},
    "address.coord" : {$lt : -65.754168}
    }
);
```

### 13) Trouver les restaurants qui ne préparent pas de cuisine "américaine" et qui ont obtenu une note "A" n'appartenant pas à l'arrondissement de Brooklyn. 

```
db.restaurants.find({
    "cuisine" : {$ne : "American "},
    "grades.grade" :"A",
    "borough": {$ne : "Brooklyn"}
}).sort({"cuisine":-1});
```

### 14) Trouver l'ID du restaurant, son nom, son quartier et sa cuisine pour les restaurants dont le nom contient les trois premières lettres de "Wil". 

```
db.restaurants.find({name: /^Wil/},
                        {
                        "restaurant_id" : 1,
                        "name":1,"borough":1,
                        "cuisine" :1
                        }
                    );
```

### 15) Trouver l'ID du restaurant, son nom, son quartier et sa cuisine pour les restaurants dont le nom contient "ces" en trois dernières lettres. 

```
db.restaurants.find({name: /ces$/},
                        {
                        "restaurant_id" : 1,
                        "name":1,"borough":1,
                        "cuisine" :1
                        }
                    );
```
                    
### 16) Trouver l'ID du restaurant, son nom, son quartier et sa cuisine pour les restaurants dont le nom contient "Reg" en trois lettres.  

```
db.restaurants.find({"name": /.*Reg.*/},
                        {
                        "restaurant_id" : 1,
                        "name":1,"borough":1,
                        "cuisine" :1
                        }
                    );
```

### 17) Trouver les restaurants qui appartiennent à l'arrondissement du Bronx et qui préparent des plats américains ou chinois. 

```
db.restaurants.find({ "borough": "Bronx" , $or : [
                                                { "cuisine" : "American " },
                                                { "cuisine" : "Chinese" }
                                                ]
                    });
```

### 18) Trouver l'ID, le nom, l'arrondissement et la cuisine des restaurants appartenant à l'arrondissement de Staten Island, Queens ou Bronx ou Brooklyn. 

```
db.restaurants.find({"borough" :{$in :["Staten Island","Queens","Bronx","Brooklyn"]}},
                                        {
                                        "restaurant_id" : 1,
                                        "name":1,"borough":1,
                                        "cuisine" :1
                                        }
                    );
```

### 19) Trouver l'ID, le nom, l'arrondissement et la cuisine des restaurants qui n'appartiennent pas à l'arrondissement de Staten Island, Queens ou Bronxor Brooklyn. 

```
db.restaurants.find({"borough" :{$nin :["Staten Island","Queens","Bronx","Brooklyn"]}},
                                        {
                                        "restaurant_id" : 1,
                                        "name":1,"borough":1,
                                        "cuisine" :1
                                        }
                    );
```

### 20) Trouver l'ID du restaurant, son nom, son quartier et sa cuisine pour les restaurants qui ont obtenu un score inférieur ou égal à 10. 

```
db.restaurants.find({"grades.score" : { $not: {$gt : 10}}},
                                        {
                                        "restaurant_id" : 1,
                                        "name":1,"borough":1,
                                        "cuisine" :1
                                        }
                    );
```

### 21) Trouver l'ID, le nom, l'arrondissement et la cuisine du restaurant pour les restaurants qui préparent des plats autres que "américain" et "chinois" ou dont le nom commence par la lettre "Wil". 

```
db.restaurants.find({$or: [
                            {name: /^Wil/}, 
                            {"$and": [
                                {"cuisine" : {$ne :"American "}}, 
                                {"cuisine" : {$ne :"Chinees"}}
                            ]}
                        ]}
,{"restaurant_id" : 1,"name":1,"borough":1,"cuisine" :1}
);
```

### 22) Trouver l'ID, le nom et les notes des restaurants qui ont obtenu la note "A" et la note 11 sur une ISODate "2014-08-11T00:00:00Z" parmi de nombreuses dates d'enquête. 

```
db.restaurants.find( 
                {
                 "grades.date": ISODate("2014-08-11T00:00:00Z"), 
                 "grades.grade":"A" , 
                 "grades.score" : 11
                }, 
                {"restaurant_id" : 1,"name":1,"grades":1}
             );
```

### 23) Trouver l'ID, le nom et les notes des restaurants dont le 2ème élément du tableau des notes contient une note de "A" et un score de 9 sur une ISODate "2014-08-11T00:00:00Z". 

```
db.restaurants.find( 
                      { "grades.1.date": ISODate("2014-08-11T00:00:00Z"), 
                        "grades.1.grade":"A" , 
                        "grades.1.score" : 9
                      }, 
                       {"restaurant_id" : 1,"name":1,"grades":1}
                   );
```

### 24) Trouver l'ID, le nom, l'adresse et la situation géographique des restaurants dont le deuxième élément du tableau coord contient une valeur supérieure à 42 et inférieure ou égale à 52. 

```
db.restaurants.find( 
                      { 
                        "address.coord.1": {$gt : 42, $lte : 52}
                      },
                        {"restaurant_id" : 1,"name":1,"address":1,"coord":1}
                   );
```

### 25) Organiser le nom des restaurants dans l'ordre croissant avec toutes les colonnes. 

```
db.restaurants.find().sort({"name":1});
```

### 26) Organiser le nom des restaurants en ordre décroissant avec toutes les colonnes.

```
db.restaurants.find().sort(
                          {"name":-1}
                          );
```

### 27) Organiser le nom de la cuisine par ordre croissant et pour ce même arrondissement de cuisine, il doit être dans l'ordre décroissant. 

```
db.restaurants.find().sort(
                           {"cuisine":1,"borough" : -1,}
                          );
```

### 28) Ecrivez une requête MongoDB pour savoir si toutes les adresses contiennent la rue ou non.

```
db.restaurants.find(
                     {"address.street" : 
                         { $exists : true } 
                     } 
                   );
```

### 29) Selectionner tous les documents de la collection restaurants dont la valeur du champ coord est double. 

```
db.restaurants.find(
                    {"address.coord" : 
                       {$type : 1}
                    }
                   );
```

### 30) Trouver le nom du restaurant, l'arrondissement, la longitude et l'attitude et la cuisine des restaurants qui contiennent «Mad» comme trois premières lettres de son nom. 

